var exec = require('child_process').exec;
function puts(error, stdout, stderr) { console.log(stdout) };

var os = require('os');
//control OS
//then run command depending on the OS

if (os.type() === 'Linux')
    exec("node scripts/build-linux.js", puts);
else if (os.type() === 'Windows_NT')
    exec("node scripts/build-windows.js", puts);
else
    throw new Error("Unsupported OS found: " + os.type());
