const path = require('path');

const VueLoaderPlugin = require('vue-loader/lib/plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    entry: './src/front-end/build.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, './src/front-end/dist'),
        publicPath: '/opa/static/dist/'
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            // this will apply to both plain `.css` files
            // AND `<style>` blocks in `.vue` files
            {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'sass-loader'//,
                    // {
                    //     loader: 'postcss-loader',
                    //     options: {
                    //       sourceMap: options.sourceMap
                    //     }
                    // }
                ]
            }
        ]
    },
    plugins: [
        // make sure to include the plugin for the magic
        new VueLoaderPlugin(),
        new CleanWebpackPlugin(['dist'])
    ]
};