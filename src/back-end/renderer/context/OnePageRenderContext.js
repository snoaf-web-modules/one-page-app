'use strict';

let merge = require('deepmerge');

let core        = require('core-libs');
let CoreContext = core.context;

class OnePageRenderContextFactory {

    constructor() {}

    init(req) {
        let selectorCtx = new OnePageRenderContextMixin(req);
        let merged = merge(req.ctx, selectorCtx);

        req.ctx = merged;
        return merged;
    }
}

class OnePageRenderContextMixin extends CoreContext {

    constructor(req) {
        super(req);
    }
}

module.exports.factory = new OnePageRenderContextFactory();
module.exports.mixin = OnePageRenderContextMixin;