'use strict';

let express = require('express');

let core        = require('core-libs');
let bootstrap   = core.bootstrap;
let logger      = core.loggerFactory.getLogger(__filename);
let CoreRouter  = core.router;
let renderEngine = core.renderEngine;

let htmlRouter      = require('./HtmlRouter');

class OnePageMainRouter extends CoreRouter {
    init() {
        return super.init('opa')
            .then(() => {
                this.install();
                return Promise.resolve();
            })
            .catch((err) => {
                logger.error('Could not setup Router, err:', err);
                return Promise.reject(err);
            });
    }

    install() {
        this.app.engine('html', renderEngine.getHtmlEngine);
        this.app.engine('bundle', renderEngine.getBundleEngine);

        // Install routes (order important)
        this.app
            .use('/static/dist',     express.static(renderEngine.getStaticPath('opa', 'dist')))
            .use('/static/js',       express.static(renderEngine.getStaticPath('opa', 'js')))
            .use('/static/css',      express.static(renderEngine.getStaticPath('opa', 'css')))
            .use('/static/fonts',    express.static(renderEngine.getStaticPath('opa', 'fonts')))
            .use('/static/models',    express.static(renderEngine.getStaticPath('opa', 'models')))
            .use('/static/images',   express.static(renderEngine.getImagePath('opa')))
            .use('',                 express.static(renderEngine.getStaticImagePath('opa', 'favicons')))

            .use('/*', htmlRouter.getRouter())
        ;
    }
}

let _singleton = new OnePageMainRouter();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;