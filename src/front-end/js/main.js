import App from './vue/app.vue';
import router from './router.js';

Vue.config.productionTip = false;

new Vue({
    el: '.app',
    router,
    components: { App },
    template: '<App/>'
});






window.onscroll = () => scrollFunction();

var pageHeader = document.getElementById("app");

function scrollFunction() {
    if (window.pageYOffset > 40)
    {
        pageHeader.classList.add("sticky")
    }
    else
    {
        pageHeader.classList.remove("sticky");
    }
}