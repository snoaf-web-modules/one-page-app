var svgLoadCount = function() {
    return {
        _countList: [],
        _listenerCallback: function () {},

        addCounter: function() {
            this._countList.push(0);
            return;
        },

        removeCounter: function() {
            this._countList.pop();
            if (this._countList.length == 0)
            {
                this._listenerCallback();
            }
            return;
        },

        registerListener(cb) {
            this._listenerCallback = cb;
        }
    };
}();

jQuery('img.svg').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    svgLoadCount.addCounter();

    jQuery.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg.addClass(imgClass + ' replaced-svg')
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Replace image with new SVG
        $img.replaceWith($svg);

        setTimeout(function() {
            $svg.addClass('loaded');
            svgLoadCount.removeCounter();
        }, 1);

    }, 'xml');

});